
from flask import Flask
from socket import gethostname
from os import environ
from uuid import uuid4

app = Flask(__name__)


@app.route("/hostname")
def get_hostname():
    return os.environ.get('HOSTNAME', str(gethostname()))


@app.route("/author")
def get_author():
    return os.environ.get('AUTHOR', 'Egor')


@app.route("/id")
def get_id():
	return os.environ.get('UUID', str(uuid4()))


if __name__ == '__main__':
    app.config["AUTHOR"] = "Egor"
    app.config["UUID"] = "e1d35322-1b07-4a7b-9502-99e32262fc7f"
    app.run(port=8000, host="0.0.0.0")
